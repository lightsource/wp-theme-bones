<?php

declare(strict_types=1);

namespace WPThemeBones;

use LightSource\ThunderWP\ThunderWP;

require_once __DIR__ . '/vendors/vendor/autoload.php';

$thunderWP = new ThunderWP();
$thunderWP->loadFromFile(__DIR__ . '/thunder-wp.php');
