<?php

use DI\Container;
use LightSource\DataTypes\Interfaces\DataTypesInterface;
use LightSource\ThunderWP\{AcfBlockAssets,
    DataTypes,
    FrontCleaner,
    Interfaces\ThemeInterface,
    Interfaces\WpObjects\MenuDataInterface,
    Interfaces\WpObjects\MenuItemInterface,
    Interfaces\WpObjects\MenusInterface,
    Templates,
    WooTemplates,
    WpObjects\MenuData,
    WpObjects\MenuItem,
    WpObjects\Menus
};
use LightSource\ThunderWP\Factories\{AcfBlocksFactory, AcfGroupsFactory, FrontBlocksFactory, LoggerFactory};
use LightSource\ThunderWP\Interfaces\ImagesInterface;
use WPThemeBones\AcfGroups\Page;
use WPThemeBones\AcfGroups\SiteSettings;
use WPThemeBones\Blocks\Defaults\Defaults;
use WPThemeBones\Blocks\Page\Theme\{Classic\PageThemeClassic,
    NotFound\PageThemeNotFound,
    PrivacyPolicy\PageThemePrivacyPolicy
};
use WPThemeBones\Images;
use WPThemeBones\Theme;

$container = new Container();

return [
    'thunder' => [
        'container' => $container,
        'instances' => [

            //////// framework

            LoggerFactory::class,
            FrontBlocksFactory::class,
            AcfGroupsFactory::class,
            AcfBlocksFactory::class,
            AcfBlockAssets::class,
            MenuDataInterface::class => MenuData::class,
            MenuItemInterface::class => MenuItem::class,
            MenusInterface::class => Menus::class,
            ImagesInterface::class => Images::class, // custom
            DataTypesInterface::class => DataTypes::class,
            ThemeInterface::class => Theme::class, // custom
            Templates::class,
            WooTemplates::class,
            FrontCleaner::class,

            //////// app

            SiteSettings::class, // AcfGroup
            Page::class, // AcfGroup
        ],
    ],
    'logger' => [
        'folder' => __DIR__ . '/logs',
        'notificationSettings' => SiteSettings::class,
    ],
    'frontBlocks' => [
        'paths' => [
            [
                'namespace' => 'WPThemeBones\Blocks',
                'folder' => __DIR__ . '/src/Blocks',
            ],
        ],
    ],
    'acfGroups' => [
        'paths' => [
            [
                'namespace' => 'WPThemeBones\AcfGroups',
                'folder' => __DIR__ . '/src/AcfGroups',
                'phpFileRegex' => '/.php$/',
            ],
            [
                'namespace' => 'WPThemeBones\Blocks',
                'folder' => __DIR__ . '/src/Blocks',
                'phpFileRegex' => '/Data\.php$/',
            ],
        ],
    ],
    'acfBlocks' => [
        'pathToBlockPages' => __DIR__ . '/assets/block-pages',
        'urlOfBlockPages' => get_stylesheet_directory_uri() . '/assets/block-pages',
        'phpFileRegex' => '/(?<!Data)\.php$/',
    ],
    'acfBlockAssets' => [
        'settings' => SiteSettings::class,
        'page' => Page::class,
    ],
    'images' => [
        'folder' => __DIR__ . '/assets/images',
        'excludedSizes' => [
            '1536x1536',
            '2048x2048',
            'medium_large',
            'woocommerce_thumbnail',
            'woocommerce_single',
            'woocommerce_gallery_thumbnail',
        ],
    ],
    'dataTypes' => [
        'isStripSlashes' => true,
    ],
    'theme' => [
        'supports' => [
            'menus',
            'woocommerce',
        ],
        'optionPages' => [
            [
                'page_title' => 'Site Settings',
                'menu_title' => 'Site Settings',
                'menu_slug' => 'site-settings',
                'capability' => 'edit_posts',
                'redirect' => false,
            ],
        ],
        // todo
        'fontsToPreload' => [
            get_stylesheet_directory_uri() . '/assets/fonts/nunito_300-700.woff2',
        ],
    ],
    'templates' => [
        'defaultsBlock' => Defaults::class,
        'page' => SiteSettings::class,
        'custom' => [
            'privacy-policy.php' => 'Privacy Policy',
        ],
        'supported' => [
            'page' => PageThemeClassic::class,
            'home' => PageThemeClassic::class,
            'category' => function () {
                wp_redirect(get_site_url());
                exit;
            },
            'author' => function () {
                wp_redirect(get_site_url());
                exit;
            },
            'single' => function () {
                wp_redirect(get_site_url());
                exit;
            },
            '404' => PageThemeNotFound::class,
            'search' => function () {
                wp_redirect(get_site_url());
                exit;
            },
            'privacy-policy.php' => PageThemePrivacyPolicy::class,
        ],
    ],
    'wooTemplates' => [
        'supported' => [],
    ],
    'frontCleaner' => [
        'wp' => [
            'blocksStyles',
            'emojiStyles',
            'duotone',
            'versionInHead',
        ],
        'yoast' => [
            'versionInHead',
        ],
        'wooCommerce' => [
            'styles',
            'assetsOnUnrelatedPages',
        ],
        'contactForm7' => [
            'styles',
            'assetsOnUnrelatedPages',
            'autoTop',
        ],
    ],
];
