## WordPress Theme Bones

### What is it

Start theme for the [ThunderWP framework](https://gitlab.com/lightsource/thunder-wp).

### How to use

1. Clone this repository
2. Replace `WPThemeBones` in files with your namespace and run `composer install; composer dump-autoload` in the vendors folder
3. Run 'yarn install' in the 'node-js' folder
4. Search for `todo` and replace values to yours

Notes:

**a) Don't forget to setup the right image sizes for the new project.**

In `Settings->Media`.

They can be various depending on the
project, as a base
you can use this: 400 - 720 - 1200.

**b) Disable post comments, if you don't need it.**

(To avoid spam via POST sends.) 

In `Settings -> Discussion`

Disable comments & pingbacks (all the three checkboxes)

### Dependencies

The ThunderWP framework requires the [ACF PRO](https://www.advancedcustomfields.com/) plugin. You can find the
recommended plugins list
[here](https://gitlab.com/lightsource/thunder-wp#7-plugins-related-packages).

### Overview

You can find all the details one the [framework's page](https://gitlab.com/lightsource/thunder-wp), along with links to
packages, from which the framework consists. Below is a short overview:

#### 1. Entrance point and config file

`function.php` is en entrance point, that creates the framework instance and loads it from the `thunder-wp.php` config
file.

`['thunder']['instances']` list is a class list, which instances will be created automatically.
If you define the key (Interface), then the created instance will be saved to the container under the key interface.

Besides the creation, it can do the following:

* For instances that implements `HooksInterface`: calls `setHooks()`
* For instances that implements `ModuleInterface`: calls `getConfigName()`, gets an option from the config with this
  name
  and passes to the `setConfigArguments()` method
* For instances that implements `FactoryInterface`: calls `makeInstance()`, then calls `getContainerProperties()` and
  saves the response to the container

#### 2. AcfGroups feature. `src/AcfGroups` folder

Classes here are turned into ACF groups automatically, class fields into ACF fields. To load values from the DB using
the `load()` method. For more details see the [AcfGroups package](https://gitlab.com/lightsource/acf-groups).

#### 3. Blocks feature. `src/Blocks` folder

**3.1) What are blocks?**

All HTML of the website (front) spit into modules. Each folder is a module, that has own markup (`.twig`) and
assets (`.scss`, `.ts`). Module by the fact is just a BEM block, so if you're familiar with the BEM methodology, you'll
easy get it.

Blocks can dependent on each other (`Menu` can include `Link`), and have Themes (`menu--theme--classic`
and `menu--theme--small`).

For more details see the [FrontBlocks package](https://gitlab.com/lightsource/front-blocks).

**3.2) What are ACF blocks?**

Blocks easily can be turned info Gutenberg components (`const IS_SUPPORT_GUTENBERG = true;`). It uses the ACF blocks
feature to signup the block in the Gutenberg library.

Along with the AcfGroups
feature (see above), it gives an easy way to create editable Gutenberg components. So we just create a class for a
Block, then create a class for an ACF Group, and assign the Group class to the Block (with the ACF location rule).

Then during rendering the `loadByAcf()` method is called in our block, where we load our Group class and initialize
fields of our Block class from these data. That's it. For
more details see the [AcfBlocks](https://gitlab.com/lightsource/acf-blocks).

**3.3) What about assets combining?(`.css` and `.js`)**

Assets combining is done by the framework on a fly.

So during a page request, happens rendering (e.g. `the_content`
parses
Gutenberg blocks and runs callbacks), and in the background, the FrontBlocks package saves a list of the rendered
blocks.

Then the AcfBlocks package combines their assets into a single file, and includes into the requested page. Combined
CSS included inline (in the `head` tag), combined JS as a separate file in the footer (bottom of the page).

#### 4. How does happen compilation of `.scss` and `.ts` files?

Our blocks are modular, it means every file must have its own output during compilation. For this goal is used
the `@lightsourcee/front-block-assets` npm package, that is wrapper on the [LaravelMix](https://laravel-mix.com/)
itself.

To compile `.scss` into `.css` and `.ts` into `.js` enter the `node-js` folder and run the following command
(make sure you've run `yarn install` before):

`yarn build-dev` (for the development environment)

`yarn build-prod` (for the production environment)

That's it. For more details see the [FrontBlocksAssets package](https://gitlab.com/lightsource/front-block-assets).

### Summary

ThunderWP framework loads from the config file, `['thunder']['instances']` is a list of classes, and these instances
will be created automatically.

You can edit existing Groups `.php`, and Block's `.php` and `.twig` files directly, and the changed will be visible
immediately.

To change assets (`.scss`, `.ts`) you need to modify the files, and make the compilation (using the `yarn build-prod`
command).