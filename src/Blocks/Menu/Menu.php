<?php

declare(strict_types=1);

namespace WPThemeBones\Blocks\Menu;

use LightSource\ThunderWP\WpObjects\Menus;
use WPThemeBones\AcfBlock;
use WPThemeBones\Blocks\Link\Link;

class Menu extends AcfBlock
{
    protected array $menusData;
    protected Link $navTopLink;
    protected Link $navLink;

    public function loadByAcf(MenuData $menuData, Menus $menus): void
    {
        $this->load();

        $menuData->load();

        $this->menusData = $menus->getMenuTwigArguments(
            $menuData->menuSlug,
            $this->navTopLink,
            $this->navLink,
            $this->navLink
        );
    }
}
