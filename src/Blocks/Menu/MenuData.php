<?php

declare(strict_types=1);

namespace WPThemeBones\Blocks\Menu;

use WPThemeBones\AcfGroup;
use WPThemeBones\Blocks\Menu\Theme\Classic\MenuThemeClassic;
use WPThemeBones\Blocks\Menu\Theme\Footer\MenuThemeFooter;

class MenuData extends AcfGroup
{
    /**
     * @a-main 1
     */
    public string $menuSlug;

    protected static function getLocationRules(): array
    {
        return [
            [
                'block == acf/' . MenuThemeClassic::getAcfName(),
            ],
            [
                'block == acf/' . MenuThemeFooter::getAcfName(),
            ],
        ];
    }
}
