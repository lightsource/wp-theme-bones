<?php

declare(strict_types=1);

namespace WPThemeBones\Blocks\Page;

use LightSource\ThunderWP\Interfaces\TemplateBlockInterface;
use WPThemeBones\AcfBlock;

class Page extends AcfBlock implements TemplateBlockInterface
{
    protected string $content;

    protected function getContent(): string
    {
        ob_start();
        while (have_posts()) {
            the_post();

            the_content();
        }

        return ob_get_clean();
    }

    public function loadByTemplate(string $template): void
    {
        $this->load();

        $this->content = $this->getContent();
    }
}
