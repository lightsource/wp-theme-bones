<?php

declare(strict_types=1);

namespace WPThemeBones\Blocks\Defaults;

use WPThemeBones\AcfBlock;
use WPThemeBones\Blocks\InstantPage\InstantPage;

class Defaults extends AcfBlock
{
    protected InstantPage $instantPage;
}
