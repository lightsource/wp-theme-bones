<?php

declare(strict_types=1);

namespace WPThemeBones\Blocks\CookieBar;

use WPThemeBones\AcfGroup;
use WPThemeBones\AcfGroups\ImageData;

class CookieBarData extends AcfGroup
{
    public string $message;
    public string $acceptLabel;
    public ImageData $closeIcon;
}
