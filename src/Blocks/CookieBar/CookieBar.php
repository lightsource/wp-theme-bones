<?php

declare(strict_types=1);

namespace WPThemeBones\Blocks\CookieBar;

use WPThemeBones\AcfBlock;
use WPThemeBones\Blocks\Catalyst\Catalyst;
use WPThemeBones\Blocks\Cookies\Cookies;
use WPThemeBones\Blocks\Link\Link;
use WPThemeBones\Images;

class CookieBar extends AcfBlock
{
    protected string $message;
    protected Link $acceptButton;
    protected string $closeIcon;

    protected Catalyst $catalyst;
    protected Cookies $cookies;

    public function loadByAcfData(CookieBarData $cookieBarData, Images $images): self
    {
        $this->load();

        $this->message = $cookieBarData->message;
        $this->acceptButton->loadByLink('#', $cookieBarData->acceptLabel);
        $this->closeIcon = $images->getAttachmentInlineSvg($cookieBarData->closeIcon->imageId);

        return $this;
    }
}
