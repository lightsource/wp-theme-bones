import dependency from '../Dependency/Dependency'

let catalyst = window._Catalyst
let cookies = window._Cookies

dependency.exists(catalyst, '_Catalyst')
dependency.exists(cookies, '_Cookies')

const COOKIE_BAR = {
    class: {
        __ACTIVE: 'cookie-bar--active',
        ACCEPT: 'cookie-bar__accept',
        CLOSE: 'cookie-bar__close',
    },
    COOKIE_NAME: '_cookie-banner',
}

@catalyst.controller
class CookieBarElement extends catalyst.class {
    setup(): void {
        super.setup()

        if (cookies.get(COOKIE_BAR.COOKIE_NAME)) {
            // remove the element from the DOM.
            // It can't be done on the server side due to server caching
            this.remove()
            return
        }

        this.querySelector('.' + COOKIE_BAR.class.ACCEPT).addEventListener('click', (event) => {
            // avoid visiting a link
            event.preventDefault()

            this.hide()
        })

        this.querySelector('.' + COOKIE_BAR.class.CLOSE).addEventListener('click', this.hide.bind(this))

        // this.show()
        setTimeout(this.show.bind(this), 5000)
    }

    protected show(): void {
        // with CSS animation
        this.classList.add(COOKIE_BAR.class.__ACTIVE)
    }

    protected hide(): void {
        cookies.set(COOKIE_BAR.COOKIE_NAME, '1', {expires: 365})

        // hide the element, without removing (as it won't be smooth)
        // element will be excluded from DOM since the next page load
        this.classList.remove(COOKIE_BAR.class.__ACTIVE)
    }
}
