<?php

declare(strict_types=1);

namespace WPThemeBones\Blocks\Example;

use WPThemeBones\AcfGroup;
use WPThemeBones\AcfGroups\ImageData;

////////////////////

function test2()
{
    /* box:  socialLinks:array, bio:block, background:image, content(title, description).
 Inner wraps everything */

    class BoxData extends AcfGroup
    {
        public ImageData $image;
        /**
         * @var []
         * @item \GreenLab\Blocks\
         */
        public array $socialLinks;
        public $bio;
        /**
         * @a-type tab
         */
        public bool $content;
        public string $title;
        /**
         * @a-type wysiwyg
         */
        public string $description;

        protected static function getLocationRules(): array
        {
            return [
                [
                    'block == acf/' . BoxThemeClassic::getAcfName(),
                ],
            ];
        }
    }

    /* header: logo:image, menuItems:array, contactButton:block, menuIcon:image, mobileMenuIcon:image. */

    class HeaderData extends AcfGroup
    {
        public ImageData $logo;
        /**
         * @var []
         * @item \GreenLab\Blocks\
         */
        public array $menuItems;
        public $contactButton;
        public ImageData $menuIcon;
        public ImageData $mobileMenuIcon;

        protected static function getLocationRules(): array
        {
            return [
                [
                    'block == acf/' . HeaderThemeClassic::getAcfName(),
                ],
            ];
        }
    }

    /* featured-item: image:image, items:array, content(title, description).
 Inner wraps everything, content wraps everything except image*/

    class FeaturedItemData extends AcfGroup
    {
        public ImageData $image;
        /**
         * @var []
         * @item \GreenLab\Blocks\
         */
        public array $items;
        /**
         * @a-type tab
         */
        public bool $content;
        public string $title;
        /**
         * @a-type wysiwyg
         */
        public string $description;

        protected static function getLocationRules(): array
        {
            return [
                [
                    'block == acf/' . FeaturedItemThemeClassic::getAcfName(),
                ],
            ];
        }
    }
}

////////////////////

/* example: */

