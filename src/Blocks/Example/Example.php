<?php

declare(strict_types=1);

namespace WPThemeBones\Blocks\Example;

use WPThemeBones\AcfBlock;
use WPThemeBones\Images;

////////////////////

class Example
{

}

function test()
{
    class BoxData
    {

    }

    class HeaderData
    {

    }

    class FeaturedItemData
    {

    }

    /* box:  socialLinks:array, bio:block, background:image, content(title, description).
Inner wraps everything */

    class Box extends AcfBlock
    {
        protected string $title;
        protected string $description;
        protected array $socialLinks;
        protected $socialLink;
        protected $bio;
        protected string $background;

        public function loadByAcf(BoxData $boxData, Images $images): void
        {
            $this->load();

            $boxData->load();

            $this->title = $boxData->title;
            $this->description = $boxData->description;

            foreach ($boxData->socialLinks as $socialLinkData) {
                $socialLink = $this->socialLink->getDeepClone();
                $socialLink->loadByAcfData($socialLinkData);

                $this->socialLinks[] = $socialLink;
            }

            $this->bio->loadByAcfData($boxData->bio);
            $this->background = $images->getImageTag($boxData->background, Images::SIZE_LARGE);
        }
    }

    /* header: logo:image, menuItems:array, contactButton:block, menuIcon:image, mobileMenuIcon:image. */

    class Header extends AcfBlock
    {
        protected string $logo;
        protected array $menuItems;
        protected $menuItem;
        protected $contactButton;
        protected string $menuIcon;
        protected string $mobileMenuIcon;

        public function loadByAcf(HeaderData $headerData, Images $images): void
        {
            $this->load();

            $headerData->load();

            $this->logo = $images->getImageTag($headerData->logo, Images::SIZE_LARGE);

            foreach ($headerData->menuItems as $menuItemData) {
                $menuItem = $this->menuItem->getDeepClone();
                $menuItem->loadByAcfData($menuItemData);

                $this->menuItems[] = $menuItem;
            }

            $this->contactButton->loadByAcfData($headerData->contactButton);
            $this->menuIcon = $images->getImageTag($headerData->menuIcon, Images::SIZE_LARGE);
            $this->mobileMenuIcon = $images->getImageTag($headerData->mobileMenuIcon, Images::SIZE_LARGE);
        }
    }

    /* featured-item: image:image, items:array, content(title, description).
Inner wraps everything, content wraps everything except image*/

    class FeaturedItem extends AcfBlock
    {
        protected string $image;
        protected string $title;
        protected string $description;
        protected array $items;
        protected $item;

        public function loadByAcf(FeaturedItemData $featuredItemData, Images $images): void
        {
            $this->load();

            $featuredItemData->load();

            $this->image = $images->getImageTag($featuredItemData->image, Images::SIZE_LARGE);
            $this->title = $featuredItemData->title;
            $this->description = $featuredItemData->description;

            foreach ($featuredItemData->items as $itemData) {
                $item = $this->item->getDeepClone();
                $item->loadByAcfData($itemData);

                $this->items[] = $item;
            }
        }
    }
}

////////////////////

/* example: */
