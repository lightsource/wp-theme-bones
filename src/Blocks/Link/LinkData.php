<?php

declare(strict_types=1);

namespace WPThemeBones\Blocks\Link;

use WPThemeBones\AcfGroup;

class LinkData extends AcfGroup
{
    /**
     * @a-main 1
     */
    public string $title;
    /**
     * @label Page
     * @a-type post_object
     * @return_format id
     * @allow_null 1
     */
    public int $pageId;
    public string $url;
    public bool $isBlank;

    public function getUrl(): string
    {
        return $this->pageId ?
            (string)get_the_permalink($this->pageId) :
            $this->url;
    }
}
