<?php

declare(strict_types=1);

namespace WPThemeBones\Blocks\Link;

use LightSource\ThunderWP\Interfaces\WpObjects\MenuItemBlockInterface;
use LightSource\ThunderWP\Interfaces\WpObjects\MenuItemInterface;
use WPThemeBones\AcfBlock;
use WPThemeBones\AcfGroups\ImageData;
use WPThemeBones\Images;

class Link extends AcfBlock implements MenuItemBlockInterface
{
    protected string $url;
    protected string $target;
    protected string $title;
    protected string $beforeIcon;
    protected string $afterIcon;

    public function __construct(private Images $images)
    {
        parent::__construct();

        $this->target = '_self';
    }

    public function loadByLink(string $url, string $title): void
    {
        $this->load();

        $this->url = $url;
        $this->title = $title;
    }

    protected function setTarget(bool $isBlank): self
    {
        $this->target = $isBlank ?
            '_blank' :
            '_self';

        return $this;
    }

    protected function setAfterSvgIcon(string $svg): self
    {
        $this->afterIcon = $this->images->getThemeInlineSvg(
            $svg,
            'link__icon link__icon--type--after'
        );

        return $this;
    }

    // e.g. menu link icon
    public function setBeforeImageAsInlineSvg(ImageData $icon): self
    {
        $pathToImage = wp_get_original_image_path($icon->imageId) ?: '';

        if (!$pathToImage) {
            return $this;
        }

        $this->beforeIcon = $this->images->getInlineSvg($pathToImage, 'link__icon link__icon--type--before');

        return $this;
    }

    public function loadByAcfData(LinkData $linkData): void
    {
        $this->load();

        $this->title = $linkData->title;
        $this->url = $linkData->getUrl();
        $this->setTarget($linkData->isBlank);
    }

    public function loadByMenuItem(MenuItemInterface $menuItem): void
    {
        $this->loadByLink($menuItem->getUrl(), $menuItem->getTitle());
        $this->setTarget($menuItem->isBlank());

        if ($menuItem->isActive() ||
            $menuItem->isChildActive()) {
            $this->classes[] = 'link--active';
        }

        if ($menuItem->getChildren()) {
            // todo
            $this->setAfterSvgIcon('arrow-down.svg');
        } else {
            $this->classes[] = 'link--without-children';
        }
    }
}
