<?php

declare(strict_types=1);

namespace WPThemeBones;

use WPThemeBones\AcfGroups\ImageData;

class Images extends \LightSource\ThunderWP\Images
{
    public function getImageTag(
        ImageData $imageData,
        string $size,
        string $classes = '',
        bool $isSkipWebp = false
    ): string {
        return $this->getImage(
            $imageData->imageId,
            $size,
            $classes,
            $imageData->isWithoutLazy,
            $isSkipWebp
        );
    }
}
