<?php

declare(strict_types=1);

namespace WPThemeBones\AcfGroups;

use LightSource\ThunderWP\Interfaces\AcfBlockAssetsPageInterface;
use LightSource\ThunderWP\Interfaces\HooksInterface;
use WPThemeBones\AcfGroup;

class Page extends AcfGroup implements AcfBlockAssetsPageInterface, HooksInterface
{
    const LOCATION_RULES = [
        [
            'post_type == page',
        ],
    ];

    /**
     * @a-type textarea
     * @instructions For developers only. This code will be included to the page stylesheet. Can be used for managing spacing between elements
     */
    public string $cssCode;

    public function getCssCode(): string
    {
        return $this->cssCode;
    }

    public function setHooks(): void
    {
        // on 'acf/init' get_queried_object_id() isn't set yet
        add_action('wp', function () {
            $pageId = get_queried_object_id();

            if (!is_page($pageId)) {
                return;
            }

            $this->load($pageId);
        });
    }
}
