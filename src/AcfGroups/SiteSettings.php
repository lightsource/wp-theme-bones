<?php

declare(strict_types=1);

namespace WPThemeBones\AcfGroups;

use LightSource\Log\Log;
use LightSource\ThunderWP\Interfaces\AcfBlockAssetsSettingsInterface;
use LightSource\ThunderWP\Interfaces\HooksInterface;
use LightSource\ThunderWP\Interfaces\LoggerNotificationSettingsInterface;
use LightSource\ThunderWP\Interfaces\TemplatesPageInterface;
use WPThemeBones\AcfGroup;

class SiteSettings extends AcfGroup implements AcfBlockAssetsSettingsInterface,
                                               HooksInterface,
                                               TemplatesPageInterface,
                                               LoggerNotificationSettingsInterface
{
    const LOCATION_RULES = [
        [
            'options_page == site-settings',
        ],
    ];

    /**
     * @a-type tab
     */
    public bool $general;
    public bool $isStaging;
    public string $productionVersion;
    /**
     * @instructions Notification about important log records will be sent from this email
     */
    public string $loggerNotificationFromEmail;
    /**
     * @instructions Notification about important log records will be sent to these emails (use a comma as a separator)<br> Note: The feature is ignored if 'isStaging' is enabled
     */
    public string $loggerNotificationToEmails;

    /**
     * @a-type tab
     */
    public bool $templates;
    /**
     * @label Header Post
     * @a-type post_object
     * @post_type page
     * @post_status private
     * @return_format id
     */
    public int $headerPostId;
    /**
     * @label Footer Post
     * @a-type post_object
     * @post_type page
     * @post_status private
     * @return_format id
     */
    public int $footerPostId;

    public function getAssetsVersion(): string
    {
        return $this->productionVersion;
    }

    public function isStaging(): bool
    {
        return $this->isStaging;
    }

    public function getHeaderPostId(): int
    {
        return $this->headerPostId;
    }

    public function getFooterPostId(): int
    {
        return $this->footerPostId;
    }

    public function setHooks(): void
    {
        add_action('acf/init', function () {
            $this->load('options');
        });
    }

    public function isLoggerNotificationEnabled(): bool
    {
        return !$this->isStaging;
    }

    public function getLoggerNotificationFromEmail(): string
    {
        return $this->loggerNotificationFromEmail;
    }

    public function getLoggerNotificationToEmails(): array
    {
        $emails = explode(',', $this->loggerNotificationToEmails);
        $emails = array_filter($emails, function ($email) {
            return '' !== trim($email);
        });

        return array_values($emails);
    }

    public function getLoggerNotificationMinLevelWeight(): int
    {
        return Log::WEIGHT_WARNING;
    }
}
