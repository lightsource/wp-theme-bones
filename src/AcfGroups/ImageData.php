<?php

declare(strict_types=1);

namespace WPThemeBones\AcfGroups;

use WPThemeBones\AcfGroup;

class ImageData extends AcfGroup
{
    /**
     * @label Image
     * @a-type image
     * @return_format id
     *
     */
    public int $imageId;
    /**
     * @instructions Useful in some cases to improve speed metrics
     */
    public bool $isWithoutLazy;
}
