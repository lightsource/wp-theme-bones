<?php

declare(strict_types=1);

namespace WPThemeBones;

use LightSource\FrontBlocks\Interfaces\RendererInterface;
use Psr\Log\LoggerInterface;
use WPThemeBones\Blocks\Animation\Animation;

class Theme extends \LightSource\ThunderWP\Theme
{
    public function __construct(LoggerInterface $logger, private RendererInterface $renderer)
    {
        parent::__construct($logger);
    }

    public function getAnimationClasses(bool $isEnabled, bool $isReverseOrder = false): array
    {
        if (!$isEnabled) {
            return [
                'parent' => '',
                'left' => '',
                'right' => '',
            ];
        }

        $this->renderer->addUsedBlockClass(Animation::class);

        $left = 'animation-item animation-item--type--fade-in-left';
        $right = 'animation-item animation-item--type--fade-in-right';

        return [
            'parent' => 'animation-parent',
            'left' => !$isReverseOrder ? $left : $right,
            'right' => !$isReverseOrder ? $right : $left,
        ];
    }
}
